/*
	First, we load the expressjs module into our appplcation and saved it in a variable called express.
*/
const express = require("express");

/*
	Create an application with express.js
	This creates an application that uses express and stores in a variable called app. Which makes it easier to use express.js methods in our API
*/
const app = express()

/*
	port is just a variable to contain the port number we want to designate for our new expressjs api
*/
const port = 4000;

/*

	To create a new route in expressjs, we first access from our express() module/package, our method. For get method request, access express (app), and use get()

	syntax:
		app.routemethod('/endpoint', '(req, res)=>{
				//function to handle request and response
		}')

*/


/*
	Setup for allowing the serve to handle data from requests(client)
	Allows your app to read JSON data
*/
app.use(express.json());


app.get('/', (req,res)=>{


	//res.send() ends the response and sends your response to the client
	//res.send() itself already does the res.writeHead() and res.end() automatically
	res.send("Hello World from our New ExpressJS api!")
})

/*
	Mini Activity:
		Create a get route in expressJS which will be able to send a message.
		Endpoint: /hello

		Message: Hello batch-157! 
*/

app.get('/hello', (req,res) => {

	res.send('Hello batch-157')
})

app.post('/hello', (req,res) => {
	console.log(req.body)
	res.send(`Hello, I am ${req.body.name}, I am ${req.body.age}. I live in ${req.body.address}`)
})

/*
	Mini Activity:
		Change the message that we send 'Hello, I am <name>, I am <age>. I live in <address>.'

*/

let users = []; 

app.post('/signup', (req,res) => {
	console.log(req.body)

	if (req.body.username != '' && req.body.password != '') {
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`)
	} else {
		res.send(`Please input BOTH username and password.`)
	}
})

/*
	C - POST
	R - GET
	U - PUT
	D - DELETE
*/

app.put('/change-password', (req, res) => {

	let message; 

	for (let i = 0; i < users.length; i++) {

		if (req.body.username === users[i].username) {

			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`
			break;

		} else {

			message = `User does not exist`

		}

	}

	res.send(message);

})


//S29 Activity
// Instruction:
// 1. Create a GET route that will access the "/home" route that will print out a simple message.
// 2. Process a GET request at the "/home" route using postman.

app.get('/home', (req,res) => {
	res.send('Welcome to the homepage')
})


// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// 4. Process a GET request at the "/users" route using postman.


app.get('/users', (req,res) => {
	res.send(JSON.stringify(users))
})


// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
// 6. Process a DELETE request at the "/delete-user" route using postman.


app.delete('/delete-user', (req,res) => {

	let message; 

	for (let i = 0; i < users.length; i++) {

		if (req.body.username === users[i].username) {

			users.splice(i, 1);

			message = `User ${req.body.username}'s has been deleted.`
			break;

		} else {

			message = `User does not exist`

		}

	}


	res.send(message)
})

// 7. Export the Postman collection and save it inside the root folder of our application.


// 8. Create a git repository named S29.


// 9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.


// 10. Add the link in Boodle.





/*
	app.listen() allows us to designate the correct port to our new expressjs api and once the server is running we can then run a function that runs a console.log() which contains a message that the server is running.

	.listen(portNumber, functionToDisplayAtTerminal)
*/
app.listen(port,()=>console.log(`Server is running at port ${port}`))